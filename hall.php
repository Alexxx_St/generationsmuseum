<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/lib.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

    <title>Document</title>
</head>
<body>
<div class="gui__container">
    <div class="gui__content ">

        <div class="timeline-container" id="timeline-1">
            <div class="timeline-header">
                <h2 class="timeline-header__title">Галерея Учителей</h2>
                <h3 class="timeline-header__subtitle"></h3>
            </div>
            <div class="timeline">
                <div class="timeline-item" data-text="1937 – 2012">
                    <div class="timeline__content"><img class="timeline__img" src="img/KarpovaVF.jpeg"/>
                        <h3 class="timeline__content-title">Карпова <br>Валентина Фёдоровна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">Валентина Федоровна Карпова родилась 3 декабря 1937 года в Подольском районе Московской области. После окончания школы училась в Московском городском педагогическом институте имени В.И.Ленина на факультете математики. С 1961 по 1967 год преподавала математику  в Школе рабочей молодежи №6 города Подольска. В 1967 году Валентина Федоровна пришла работать в школу № 654 и преподавала в ней математику до 2010 года.
                            Талантливый математик, прекрасный учитель, Валентина Федоровна обладала непререкаемым авторитетом среди учеников и учителей. Она увлекла своим предметом сотни ребят, помогла многим развить математические способности, подготовиться к поступлению в лучшие вузы страны – МГУ, МФТИ, МИФИ, МВТУ имени Баумана… Среди ее учеников – победители математических олимпиад различных уровней, международных математических турниров. Валентина Федоровна – инициатор создания в школе профильных математических классов (они были организованы у нас уже в 1968 году). В.Ф.Карпова  вместе с учеными участвовала в разработке программ углубленного изучения математики, сотрудничала с АПН СССР (лаборатория Ю.К.Бабанского) по проблеме оптимизации учебно -воспитательного процесса. 
                            Валентина Федоровна – один из лучших классных руководителей в школе. Она всегда была для своих подопечных и строим наставником, и заботливой мамой, и надежным добрым другом. Невозможно перечислить все  экспедиции и походы, в которые она отправлялась с ребятами, все спектакли, которые она с классами смотрела, все музеи,  куда приводила детей. В ее пионерских отрядах были самые интересные сборы,  у старшеклассников -самые интересные классные часы, у ее классов были лучшие выступления на Эстафетах искусств. Она всегда была рядом со своими учениками: на уроках, в «Бригантинске», в строительном отряде.
                            Она была лидером и растила лидеров. Она не боялась брать на себя ответственность и учила ответственности окружающих. Она была принципиальным, честным человеком и растила нравственных людей. Она  была очень требовательна к себе: для нее не было уважительной причины, чтобы опоздать или не придти на урок. В школе никто не может вспомнить ни одного больничного Валентины Федоровны – кроме ее последней, тяжелой болезни. Невозможно было не уважать этого учителя, этого классного руководителя. 
                            Многие ее выпускники стали учителями математики, трое из них  работали в нашей школе: Елена Петровна Борисова, Наталья Вячеславовна Иванова, Наталья Александровна Долгова.
                            Валентина Федоровна – лауреат конкурса «Грант Москвы»,  конкурса «Грант Сороса»,  ей присвоено звание «Почетный работник общего образования Российской Федерации». Ее главное звание, присвоенное ей учениками – любимый Учитель.
                        </p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text="1938-2013">
                    <div class="timeline__content"><img class="timeline__img" src="img/HvackiyAD.jpeg"/>
                        <h3 class="timeline__content-title">Хвацкий <br>Алексей Дмитриевич</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">
                                «Родился 7 апреля 1938 года в городе Химки… Отец - Дмитрий Наумович- ушел добровольцем на фронт и погиб под Сталинградом в 1942 году. (Память о нем Алексей Дмитриевич свято хранил. Многие выпускники помнят, как в стройотряде, в «Бригантинске» 22 июня он пел Песню  об отце: «Двадцать лет похоронная врет, двадцать лет говорит: « Не верь» . Ты живешь, ты стоишь у ворот, ты уже открываешь дверь». Незадолго до смерти, узнав, что из школы отправляется экспедиция в Волгоград, он просил привезти ему землю с Мамаева кургана…) В городе Химки закончил 4 класс на одни пятерки, 7 класс – на одни четверки, 10 класс-… ( и тройки в аттестате). С шестого класса увлекался судомоделированием, мечтал стать морским инженером, но не получилось…
                                2,5 года работал слесарем в НПО имени Лавочкина, поступал в вуз на судостроительную специальность – не судьба…
                                  В армии освоил специальность фотомеханика на аэрофотоаппаратуре и в 1960 году поступил в МИИГАИК на специальность «инженер-оптик-механик». В 1967 году окончил уже вечерний факультет этого института и имел 4-летний стаж работы в оборонных НИИ. В 1968 году пришел в 654 школу на должность учителя технологии (труда) и черчения. В 1971-1973 году работал заместителем директора по воспитательной работе. С 1968 по 1971 год был начальником лагеря «Бригантинск».
                                 В период с 1973 по 1985 г.г. был прорабом школьного стройотряда в Колесниках, Яганово, Слатино, на Куровской Маленковской, в Михнево, в Андреевском…( ученики и коллеги присвоили Алексею Дмитриевичу, самому необходимому и дорогому человекау стройотряда  звание Главнокопающего).
                                 Работал в школьных мастерских (подшефных АЗЛК) и преподавал черчение в 7-8 классах.» Свою автобиографию в 2005 году Алексей Дмитриевич закончил так :«Готов выполнить любое задание Родины». Эти слова, сказанные в конце неофициального документа, для него не были шуткой…
                                 Кто он, Алексей Дмитриевич? Инженер, ставший Учителем. Каким? Замечательным. Любимым. Он был талантлив во всем! Его руками создана наша школа: оформлены ее залы, коридоры, кабинеты музей. В начале 70-х он создал школьный радиоузел и стал наставником десятков увлеченных радио мальчишек. Он играл в школьном театре - ТЕАСЕ. Вспомните его в роли деда Женьки Столетова в спектакле «И это все о нем…» (1976год). Он прекрасно играл на гитаре и пел – это хорошо знают бойцы всех стройотрядов. Он писал чудесные стихи – это знают те, кто читал их в школьном альманахе «Алый парус», те, кому посчастливилось быть именинниками в стройотрядах, это знают его близкие. Он замечательно рисовал… Кажется, он умел все. И еще он умел выслушать. Умел пошутить. Умел помочь пережить беду. И никогда не старался быть заметным, быть первым, казаться значительным. Но мы всегда знали, что он – один из самых лучших, самых любимых Учителей. 

                                олжны быть ссылки на его стихи, песни, на сочинение о нем Глеба Кораблева.

                        </p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/GarevaTA.jpeg"/>
                        <h3 class="timeline__content-title">Гарева  <br>Тамара Алексеевна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">
                            Тамара Алексеевна Гарева родилась 15 июня 1935 года в Московской
                            области. В 1956 году Тамара Алексеевна окончила Спасское педагогическое
                            училище Приморского края и три года работала в средней школе №9 города
                            Владивостока учителем начальных классов. С 1959 по 1966 год она учила
                            малышей в средней школе №2 поселка Бутово Московской области. В 1966
                            году Тамара Алексеевна пришла работать в нашу школу и в 1967 году была
                            назначена заместителем директора по учебно-воспитательной работе в
                            начальной школе. На этой должности Тамара Алексеевна проработала до
                            1988 года, создавая систему работы начальной школы, совершенствуя
                            предметное преподавание в ней, внедряя в практику идеи оптимизации
                            учебно-воспитательного процесса. Тамара Алексеевна уделяла большое
                            внимание октябрятской работе, участвовала в создании КВЧ - Клуба
                            Веселых Человечков, была хорошим методистом, учила молодых учителей.
                            Но в памяти сотен выпускников Тамара Алексеевна Гареева осталась не
                            только прекрасным учителем. 19 лет она возглавляла работу школьного
                            лагеря «Бригантинск», стояла у истоков города, создавала и укрепляла его
                            традиции. Каждый год в июне и июле она отвечала за жизнь и здоровье, за
                            труд и отдых не менее чем 150 учеников. Она жила в палатке, спала на нарах,
                            каждое утро выезжала с ребятами на прополку свекольных полей и держала
                            под своим контролем все события жизни города. Она переживала с ребятами
                            жару и дожди, холод и шквалистые ветры, болела за команду города в
                            спортивных соревнованиях с «Москвичом», гордилась тем, что ее питомцы
                            умеют, знают больше и поют лучше, чем ребята из спортлагеря, судила
                            КВНы, подбадривала ночных дежурных, участвовала в праздниках
                            Последнего сорняка, провожала и встречала походников… Она знала и
                            любила бригантинцев; как мать, заботилась о них, была строга, справедлива
                            и душевна. Тепла ее сердца хватало на всех. Ребята платили ей любовью,
                            доверием и уважением, поэтому проблем с дисциплиной в Бригантинске не
                            было, а воспоминания о лагере трепетно хранят все его жители.
                            Большинство ее коллег отдыхали летом на курортах, а она, уложив лагерь
                            после отбоя, говорила своей «команде»: «А теперь – на дачу!» «Дачей» был
                            стол в конце лагеря. С костра приносили закопченный чайник, и Тамара
                            Алексеевна со своими единомышленниками подводила итоги дня и
                            планировала день следующий. Так проходило лето, лагерь возвращался в
                            Москву, и Тамара Алексеевна выходила на работу – надо было готовить
                            школу к 1 сентября. Кажется, она вообще не умела отдыхать…

                            Т.А.Гарева была членом КПСС и несколько лет возглавляла школьную
                            партийную организацию. Коммунистов школы было за что уважать, на них
                            хотелось равняться…
                            Тамара Алексеевна учила нас преодолевать трудности, не терять
                            оптимизма, и сама до последних лет жизни, тяжело болея, оставалась
                            сильной, шутила и подбадривала нас. Те, кто приезжал ее навестить,
                            поддержать ее, встречались с не сломленным болезнью человеком и слышали
                            от нее слова: «Любите и берегите друг друга».
                            Т.А Гарева награждена медалью «Ветеран труда», нагрудными знаками
                            «Отличник народного просвещения РСФСР», «Отличник просвещения
                            СССР». В 1984 году Тамаре Алексеевне было присвоено звание
                            «Заслуженный учитель школы РСФСР».
                        </p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text="1935 – 1990">
                    <div class="timeline__content"><img class="timeline__img" src="img/PriklonskiyAS.jpeg"/>
                        <h3 class="timeline__content-title">Приклонский <br>Алексей Сергеевич</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">
                        Алексей Сергеевич работал в школе  учителем русского языка и литературы с 1963 года. 
                        Человек неиссякаемой энергии, он всегда был занят работой,  был одним из тех, кто создавал нашу школу. Вместе со своими учениками он участвовал в строительстве здания школы на улице Артюхиной, находил  нестандартные решения для оформления учебных кабинетов.
                        Несколько лет Алексей Сергеевич проработал директором 484 школы, но сердце его принадлежало ШПЧ, и при первой же возможности он вернулся. 
                        Алексей Сергеевич много лет работал в «Бригантинске», водил ребят в походы по Подмосковью, ставил с классами спектакли, проводил интересные уроки, учил детей любить литературу. Его чувство юмора, знания, уважение к людям всегда привлекали к нему и коллег, и учеников. Он был любимым классным руководителем, о нем ходили легенды, его словечки и выражения повторяли и в учительской, и в школьных коридорах.
                        Кроме того, Алексей Сергеевич умел многое делать своими руками: он рисовал, вырезал по дереву, лепил - оформленный им кабинет литературы долгие годы удивлял и восхищал всех, кто туда входил. 
                        Он был немножко авантюристом, придумывал приключения, игры, увлекал ребят и увлекался сам.  Он был всегда доброжелательным и оптимистичным, никогда не сдавался и не опускал рук, что бы ни случилось.
                        У него просили помощи, и он не просто не отказывал – делал все, что было в его силах, чтобы молодые учителя могли научиться учить детей. Он щедро делился своим опытом, своими наработками, поддерживал тех, кто приходил в школу работать.
                        Он писал стихи, он был поэтом, как многие его друзья. Он был одним из матросов команды, которая шла  под  алыми парусами мечты, команды, создавшей в школе  уникальный дух святого братства.
                        </p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text="">
                    <div class="timeline__content"><img class="timeline__img" src="img/KrupsES.jpeg"/>
                        <h3 class="timeline__content-title">Крупс <br>Элла Станиславовна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">Когда – то в начале тридцатых годов прошлого столетия одна американская семья Крупс 
                            переехала жить из Америки  в СССР. Будучи инженерами и коммунистами, они очень хотели помочь  развитию промышленности 
                            молодой советской республики. С собой они привезли трех детей.  Одной из них была девочка Эллис Крупс. Семья в 1937 году 
                            попала под сталинские репрессии, всех судьба разбросала по разным уголкам СССР. А Эллис Крупс оказалась в Москве, училась и, наконец,  
                            стала Эллой Станиславовной Крупс, учителем английского языка в нашей школе. Здесь она работала с конца пятидесятых годов.
                            Очень часто Элла Станиславовна начинала считать деньги (одно время она была ответственной за питание в школе) на АНГЛИЙСКОМ ЯЗЫКЕ. 
                            И многие дети, которые учились у нее,  считали, что она «воображает» или хочет подчеркнуть, что она учитель английского языка. 
                            На самом деле английский язык – это ее РОДНОЙ язык . Она мыслила на нем, иногда с трудом подбирая русские слова. 
                            Ей катастрофически не хватало английского языка в жизни. Тогда в СССР нельзя было найти в свободной печати изданий на английском языке, 
                            послушать радиопередачи или посмотреть фильмы на английском. Элла Станиславовна прочитывала от корки до корки газету 
                            «Moscow News» на английском языке, а потом самый интересный материал давала учащимся на уроках.
                            Это были материалы о европейских и американских музыкальных группах, актерах и актрисах.
                            Из-за нехватки общения на родном языке Элла Станиславовна даже русскую классику читала в английских переводах.
                            Но зато, когда в нашу школу приезжали иностранные делегации (а в те времена это была не редкость), лучшего переводчика было не найти. 
                            Иностранные группы зачастую отказывались от своих штатных переводчиков в пользу Эллы Станиславовны. И тогда эту красивейшую женщину было не остановить. 
                            Она как будто парила по коридорам школы, показывая родную школу и рассказывая о ней на родном языке.
                            Элла Станиславовна была прекрасным и любимым учителем, была замечательным и любимым классным руководителем. Неравнодушная к делам школы,  
                            она ходила с ребятами в походы, ездила на экскурсии, уже в немолодом возрасте работала в строительном отряде Яганово. Прямая, оптимистичная, 
                            обладающая чувством юмора, умная и чуткая, она была другом детей, пользовалась огромным уважением в коллективе.
                        </p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/KavernayaRA.jpeg"/>
                        <h3 class="timeline__content-title">Каверная <br>Римма Анатольевна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/KapralovaVS.jpeg"/>
                        <h3 class="timeline__content-title">Капралова  <br>Валентина Стефановна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/GolovkoVI.jpeg"/>
                        <h3 class="timeline__content-title">Головко  <br>Валентина Ивановна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/KirillenkoAS.jpeg"/>
                        <h3 class="timeline__content-title">Кириленко <br>Алла Семеновна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/LebedMP.jpeg"/>
                        <h3 class="timeline__content-title">Лебедь <br>Мария Петровна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/LeskovaLT.jpeg"/>
                        <h3 class="timeline__content-title">Лескова <br>Любовь Тимофеевна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/LimonnikovaAI.jpeg"/>
                        <h3 class="timeline__content-title">Лимонникова <br>Антонина Ивановна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/MiheevaUV.jpeg"/>
                        <h3 class="timeline__content-title">Михеева <br>Юлия Владимировна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/RagulinaRS.jpeg"/>
                        <h3 class="timeline__content-title">Рагулина <br>Раиса Степановна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">Раиса Степановна – прежде всего удивительно интеллигентный человек, неизменно тактично, с искренним вниманием и добротой относящаяся как к каждому своему ученику, так и ко всем окружающим ее людям. А еще Раиса Степановна – профессионал высочайшей пробы! Она принадлежит к тем учителям, про которых говорят – творческий человек, талантливый учитель. Это мастер, учитель-виртуоз, который всегда находил  новые методические и дидактические приемы и подходы в работе с детьми. Тихий голос, ровный тон – но  с тем большим вниманием каждый  ее ученик слушает  и постигает не только грамматику русского языка, но и трудную науку уважительных человеческих взаимоотношений. Это учитель, уважающий мнение ученика, она умеет слушать и слышать, умеет подтолкнуть ученика к пониманию истинной сути не только языковых явлений, но и общечеловеческих ценностей. Раиса Степановна помогла многим ученикам понять красоту родного слова.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text="">
                    <div class="timeline__content"><img class="timeline__img" src="img/SelyanichevaNA.jpeg"/>
                        <h3 class="timeline__content-title">Селяничева <br>Нина Алексеевна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text="">
                    <div class="timeline__content"><img class="timeline__img" src="img/ShapovalovaTM.jpeg"/>
                        <h3 class="timeline__content-title">Шаповалова <br>Тамара Михайловна </h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">
                            1968 год… Тогда ей было всего 28 лет. Поработав сначала в детском саду, потом в интернате с детьми, которые были эвакуированы в Москву после землетрясения в Ташкенте , по рекомендации завуча интерната , в котором она работала , Тамара Михайловна  впервые приходит в 654 школу. Так начинался ее пятидесятилетний марафон в нашей школе. 
                            Учитель географии (иногда одна на всю школу). Уроки, классное руководство, походы по Подмосковью, эстафеты искусств, родительские собрания и снова уроки. Потом – Организатор внеклассной работы (зам.директора по воспитательной работе). Организация внеклассных мероприятий, выступления на различных конференциях, работа с Академией педагогических наук, экспедиции, стройотряды, « Бригантинск» и опять уроки. Затем - заместитель директора по учебной работе. Составление расписания, замены уроков, распределение кабинетов, представление школы на различных уровнях и, конечно, уроки. Заметьте, все это без больничных листов! Даже со сломанной ногой на такси она ездила проводить уроки! Школа, работа – всегда на первом месте. 
                            Вот так в этой круговерти и пролетели 50 лет работы в нашей школе…  Теперь только воспоминания, добрые слова выпускников и их родителей при встрече…. 
                            Вся ее жизнь – это ШПЧ!
                        </p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/SmetaninaLA.jpeg"/>
                        <h3 class="timeline__content-title">Сметанина <br>Лидия Антоновна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/ZhadinMA.jpeg"/>
                        <h3 class="timeline__content-title">Жадин <br>Михаил Алексеевич</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                

                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/BorsukOA.jpeg"/>
                        <h3 class="timeline__content-title">Борсук <br>Олег Анатольевич</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/DerilloEI.jpeg"/>
                        <h3 class="timeline__content-title">Дерило <br>Иван Ефимович</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/GrinevNP.jpeg"/>
                        <h3 class="timeline__content-title">Гринев <br>Николай Петрович</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/IvanovaNN.jpeg"/>
                        <h3 class="timeline__content-title">Иванова <br>Нина Николаевна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>

                
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/GafarovaLL.jpeg"/>
                        <h3 class="timeline__content-title">Графова <br>Людмила Львовна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/Zhuravleva.jpeg"/>
                        <h3 class="timeline__content-title">Журавлева <br>Наталья Юрьевна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/Grishkina.jpeg"/>
                        <h3 class="timeline__content-title">Гришкина <br>Татьяна Николаевна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">Учитель обществознания, философии и логики.
                            Выпускница ШПЧ, ученица Анатолия Давыдовича Фридмана.
                            Окончила Московский государственный университет имени В.И.Ленина, факультет философии. После окончания ВУЗа работала 10 лет в МИИГАиКе преподавателем политологии. Однако с детства мечтала работать учителем в школе. Сейчас педагогический стаж Татьяны Николаевны составляет чуть больше 30 лет! В 2008 году Татьяне Николаевне было присвоено звание Почетного работника общего образования. Т.Н.Гришкина является действующим экспертом по обществознанию в системе ЕГЭ. 
                            Татьяна Николаевна - это не просто учитель, это – учитель-философ, историк, обществовед, эстетик, логик! На уроках Татьяны Николаевны ученики не просто изучают предмет философии, но испытывают полное погружение в историю мысли и морали, проникая в самые глубины умов античных мыслителей и мыслителей эпохи средневековья! Ученики не просто анализируют философские постулаты и изучают законы логики и мышления, но проецируют их на исторические события и настоящее время, перекладывают на собственный опыт и подвергают личной оценке. Да – да! Именно критическое мышление и оценивание приветствуются на уроках Татьяны Николаевны в особых заданиях – сочинениях «Теоретические вопросы». 
                            Многие выпускники, которым посчастливилось учиться у Татьяны Николаевны,  отмечали, что пройденный в школе материал изучался на 1-3 курсах разных направлений в институтах. Многие говорили, что «самыми ценными школьными тетрадями были тетради с лекциями Татьяны Николаевны», благодаря которым ребятам удавалось блеснуть знаниями на семинарах и успешно сдавать зачеты «автоматом».
                        </p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/Synelshikova.jpeg"/>
                        <h3 class="timeline__content-title">Синельщикова <br>Наталья Вадимовна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/lysenko.jpeg"/>
                        <h3 class="timeline__content-title">Лысенко <br>Татьяна Николаевна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/Zhavoronkova.jpeg"/>
                        <h3 class="timeline__content-title">Жаворонкова<br> Екатерина Александровна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/ShapovalovaOB.jpeg"/>
                        <h3 class="timeline__content-title">Шаповалова<br> Ольга Борисовна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/Blinkova.jpeg"/>
                        <h3 class="timeline__content-title">Блинкова <br>Наталья Вячеславовна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/HristoforovaMA.jpeg"/>
                        <h3 class="timeline__content-title">Христовфорова<br>Марина Анатольевна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/Sukovatiy.jpeg"/>
                        <h3 class="timeline__content-title">Суковатый<br>Владимир Генадьевич</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/RebrovaNV.jpeg"/>
                        <h3 class="timeline__content-title">Реброва<br>Наталья Викторовна</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">текст Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint non natus maxime deleniti repudiandae accusantium dolores atque laboriosam dicta! Reprehenderit obcaecati recusandae, est at repudiandae maiores non iste autem fugiat.
                        Saepe magni culpa nemo nobis, impedit optio illum laborum, natus nulla, itaque inventore molestiae tempore odio maxime neque quidem consequatur iste quasi molestias odit! Tenetur ea laboriosam ab porro atque.
                        Velit illum ipsa eius laudantium optio amet excepturi veniam dicta a unde? Fuga perferendis natus, quam laboriosam, at illo animi sint reprehenderit necessitatibus dolorem omnis molestias provident odio ipsum quod.</p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>

                <div class="timeline-item" data-text=" ">
                    <div class="timeline__content"><img class="timeline__img" src="img/FridmanAD.jpeg"/>
                        <h3 class="timeline__content-title">Фридман <br> Анатолий Давыдович</h3>
                        <div class="timeline__content-desc">
                        <p class="expand expanded">Говорят, что человек в жизни, выполняя свою миссию, должен построить
                            дом, посадить дерево, вырастить сына. Мы хотим рассказать вам о том, чьей
                            биографии хватило бы на очень многих, о том, кто придумал наш дом,
                            построил два школьных здания, вдохнул в них жизнь и каждый день на
                            протяжении 40 лет отвечал за нее. О том, кто был отцом и другом для тысяч
                            мальчишек и девчонок, о том, кто посадил вокруг своих домов сад, который
                            цветет и сегодня.
                            2.
                            Анатолий Давыдович Фридман родился 9 июля 1930 года в Москве. Его
                            мама, Агриппина Герасимовна Финогенова, уроженка Владимирской
                            области, была партийным работником, папа, Давид Соломонович, прошел
                            путь от рабочего до директора Института легкой промышленности. В 1941
                            году он покинет высокий пост, чтобы добровольцем уйти в ряды народного
                            ополчения защищать Москву. В должности заместителя командира
                            артиллерийского полка он вместе с бойцами 17 дивизии народного
                            ополчения дойдет до Восточной Пруссии, закончит войну в звании
                            подполковника. После войны он будет назначен заместителем министра
                            легкой промышленности и до конца своей жизни будет выполнять
                            общественную работу составе Совета ветеранов 17 дивизии, очень много
                            сделает для сохранения памяти о народном подвиге. Например, стела на
                            Стремиловском рубеже обороны, к которой мы приезжаем ежегодно,
                            установлена по его инициативе и по его проекту.
                            Анатолий Давыдович уважал и любил родителей. Патриотизм,
                            ответственность, требовательность ими были воспитаны в сыне с детства.
                            3.
                            Учеба А.Д. пришлась на трудные военные и первые послевоенные годы. В
                            эти годы и складывалось представление А.Д. о настоящем учителе,
                            преданном детям. Классный руководитель Зоя Константиновна не просто
                            замечательно вела уроки – она целый год опекала своего ученика, в 15 лет
                            потерявшего маму (В 1945 у Агриппины Герасимовны не выдержало сердце,
                            а до возвращения отца с войны было еще далеко). Кстати, позже о пожилой
                            одинокой учительнице до конца ее жизни заботился уже А.Д.

                            Школа № 622 на Таганке дала знания и научила дружить. А.Д. был
                            комсоргом класса – отчаянным, смелым, принципиальным. Его дружба с
                            одноклассниками длилась 60 лет. Они встречались ежегодно, в 2005 класс
                            собрался на похороны своего лидера. А.Д. хорошо понимал ценность
                            школьного братства, и всеми силами создавал его в школе, которую
                            возглавил. Не случайно он обращал нас к идеалу – пушкинскому выпуску
                            лицея, не уставая повторять слова о прекрасном союзе… Не случайно после
                            упразднения в стране пионерской и комсомольской организаций он с
                            ребятами придумал союз «Молодая гвардия», который живет до сих пор…
                            4.
                            А.Д. с отличием окончил Литературный факультет Московского
                            учительского института, серьезно увлекался литературным творчеством. У
                            него были прекрасные опыты в поэзии и прозе, некоторые его
                            произведения мы уже после его смерти опубликовали в альманахе «Алый
                            парус». Наверное, во время учебы он навсегда влюбился в книги А.Грина.
                            (Кстати, ему довелось вместе с К.Паустовским разбирать архивы писателя в
                            Старом Крыму). Увлекся творчеством Экзюпери и потом сумел привить
                            любовь к романтике многим поколениям учеников. Но настоящим кумиром
                            его был все-таки Пушкин, в произведениях которого он учил нас видеть весь
                            мир. Любовь к педагогике победила. А.Д. не стал писателем, не стал актером
                            (он увлеченно играл на сцене театра Дома учителя) - стал учителем
                            литературы. Блестящим. После службы в армии А.Д. снова идет учиться,
                            теперь уже в Педагогический институт. Он начинает работать
                            пионервожатым в летних лагерях, работает учителем русского языка и
                            литературы и вскоре его назначают завучем, а потом – директором школы
                            рабочей молодежи.
                            5.
                            В августе 1965 года молодому, талантливому педагогу доверяют
                            руководство средней общеобразовательной школой № 654. Старое здание.
                            Учителя, которые гораздо старше директора. И мечта, которую он день за
                            днем начинает воплощать в жизнь, создавая коллектив единомышленников,
                            влюбляя в свою мечту и учителей, и старшеклассников. Ему помогают
                            друзья: за ним в нашу школу пришли Алексей Дмитриевич Хвацкий, Евгений
                            Борисович ФедоровскийФедоровский…

                            Какая же мечта двигала нашим директором? Мечта о школе
                            свободного выбора, о школе, которая раскроет способности и таланты
                            каждого, каждому даст возможность самовыражения, научит учиться, даст
                            нравственные ориентиры, станет опорой в жизни
                            6.
                            И уже в 1965 школа повернулась лицом к каждому ученику. Именно в
                            этом году положено начало всем главным школьным традициям. А.Д. была
                            придумана высшая награда школы – Поручительство Чести. И он от класса к
                            классу вел своих воспитанников к этой награде... Свой первый спектакль –
                            «Памяти павших» - поставил школьный ТЕАС. А.Д. создал и возглавил
                            театральную студию, стал режиссером десятков спектаклей:«Маленький
                            принц», «Герой нашего времени», «Пушкин», «Салют» «Декабристы», «Сода-
                            солнце», «И это все о нем», «Трубач на площади»… Троечники и хулиганы
                            волшебным образом преображались на сцене. Прошли годы, и из ТЕАСА
                            «вырасла» Школа творчества, работающая практически с каждым учеником
                            ШПЧ.
                            Для ребят в школе начали работу клубы Веселых человечков, Знаменитых
                            капитанов, Кибальчиш, Бригантина. Был создан отряд вожатых – шефов
                            младшеклассников, сформировались группы красных следопытов. В школе
                            активно заработало ученическое самоуправление.
                            Летом 1966 года Анатолий Давыдович организует для старшеклассников
                            палаточный лагерь на острове на Оке – город Бригантинск. Скоро мы будем
                            отмечать 50-летие любимого города. Он несколько раз менял место на карте,
                            но не изменял своему духу, своему флагу и девизу – «Бороться и искать,
                            найти и не сдаваться». 50 лет старшеклассники поют «Бригантину», давно
                            ставшую гимном школы. Бригантинск продолжает жить по уставу,
                            написанному А.Д. Организация жизни лагеря – дело трудное, хлопотное и
                            очень ответственное. Ни трудностей, ни ответственности А.Д. никогда не
                            боялся, преодолевал все преграды, если знал – это нужно ребятам. Школу
                            Бригантинска прошли несколько поколений ШПЧистов, и все они сохранили
                            любовь к этому городу.
                            7.
                            С 1971 по 1989 год продолжением Бригантинска для выпускных классов
                            становился школьный стройотряд. Его тоже придумал А.Д., будучи

                            убежденным, что десятиклассники – взрослые люди и должны чувствовать
                            это. Для директора это была еще большая ответственность: он вез своих
                            учеников на строительство железнодорожных линий связи и отвечал за их
                            жизнь и здоровье. Ребята ехали в отряд по комсомольским путевкам, и надо
                            было заслужить это доверие. Вставали в 6 утра, рыли траншеи,
                            прокладывали кабель, участвовали в строительстве железнодорожных
                            объектов. За работу на родине первого космонавта в 1973 году отряду было
                            присвоено имя Гагарина. Все, кто прошел через стройотряд, считают это
                            воспоминание одним из лучших в жизни и бережно хранят именные
                            наградные часы и удостоверения бойцов стройотряда. А А.Д. всегда и везде
                            был с отрядом, работал с ребятами в одной траншее, уезжая в школу только
                            на экзамены. В олимпийском 1980 он на поезде, приехав из Москвы, привез
                            в стройотряд под Харьковым два ящика доселе невиданной фанты, потому
                            что там намечался общий день рождения, и ему просто хотелось побаловать
                            десятиклассников…
                            8.
                            В конце шестидесятых А.Д. с учителями организовал экспедиции в города -
                            герои Брест и Волгоград. Конечно, он был там с ребятами. В школе уже очень
                            много делалось для сохранения памяти о войне, и результатом этих
                            экспедиций стало создание музея Боевой Славы. Вечерами и ночами в
                            апреле 1970 директор с учителями и старшеклассниками штукатурили,
                            красили, выкладывали кирпичи. Этот музей – один из лучших школьных
                            музеев – продолжает свою работу и спустя 45 лет. Одним из его почетный
                            гостей был К.М.Симонов, оставивший в книге свой восторженный отзыв.
                            Кстати, приглашение в школу известных писателей, ученых, спортсменов,
                            актеров – тоже инициатива А.Д.
                            В 1972 году с первого похода по местам боевой славы 17 дивизии
                            народного ополчения начинается история школьного турклуба «Вертикаль».
                            Вернее, сам турклуб родился чуть позже, тогда начались серьезные походы.
                            Нет, А.Д. сам в походах не участвовал, но их планы, маршруты, результаты
                            всегда непосредственно интересовали его. Надо признать, что в жизни
                            учеников никогда не было ничего, что было бы ему неинтересно, что
                            происходило бы без его участия. Его так и называли в Москве –
                            комсомольско-пионерский директор. Эстафеты искусств, Праздники труда
                            (именно он придумал медаль «Мастер умелые руки», добился ее

                            изготовления для ребят, такие медали хранят с семьях нескольких
                            поколений ШПЧ), комсомольские слеты, военно-спортивные игры… Не
                            перечислить всего, что он придумывал и делал для ребят. Окна в школе
                            светились до позднего вечера, здесь всегда было интересно…
                            9.
                            Школа Фридмана воспитывала и, конечно, давала достойные знания. Сам
                            А.Д. был прекрасным учителем. Его уроки литературы выпускники
                            вспоминают, уже будучи седыми людьми. А.Д. был очень хорошим
                            методистом, постоянно посещал и анализировал уроки своих коллег.
                            В первые годы работы А.Д. создал внутри школы университет «Эврика» -
                            систему предметных кружков и факультативов.
                            Уже в начале 70-х А.Д.Фридман, лет на пятнадцать опережая педагогические
                            идеи, открыл профильные классы для старшеклассников: в школе появились
                            математический и гуманитарный классы, предопределив сегодняшние 5
                            профильных направлений. В 70-е годы А.Д. привлекает в школу
                            педагогическую науку, школа получает статус лаборатории при Академии
                            педнаук и работает над проблемой оптимизации образования вместе с
                            учеными .Ему предлагают защищать диссертацию, он отказывается –
                            некогда, надо работать. Щедро делится своими открытиями, идеями. На них
                            защищают диссертации ученые. Его рабочий день – 12-13 часов, почти без
                            выходных. Он сам вместе с ребятами в 1967 и 1987 годах принимал участие
                            в проектировании двух новых зданий школы, вместе с ребятами работал на
                            их строительстве. В результате появились оборудованные всем
                            необходимым актовые залы, лекционные аудитории, две библиотеки с
                            читальными залами, несколько спортзалов, тренажёрный зал, гараж (мы
                            уже привыкли к возможности путешествовать по стране на двух собственных
                            автобусах). На месте обычного подвала сначала он организовал тир, потом
                            ценой невероятных усилий, согласований добился разрешения на
                            строительство бассейна и построил его. «Это невозможно», – говорили ему.
                            Но для него – мы уже рассказывали – невозможного не было. Он умел сказку
                            сделать былью, и у него это получалось 40 лет. Потому что была
                            придуманная им сказка и было желание ее воплотить в жизнь. И было
                            понимание того, что это необходимо детям. Одним из первых директоров он
                            начал тесно сотрудничать с вузами, привлек в школу преподавателей вузов,
                            добился возможности прямого поступления в институты выпускников

                            школы: на школьных экзаменах преподаватели институтов ставили им
                            оценки, а на выпускном вечере поздравляли с зачислением. Одной из
                            первых в 1991 году наша школа получила статус гимназии. Спустя несколько
                            лет, когда в городе определили статус гимназии как учебного заведения 5-11
                            классов, А.Д. отказался от него, не представляя нашу школу без «началки».

                            За 40 лет работы А.Д. получил несчетное количество выговоров за
                            многочисленные нарушения различных запретов и постановлений, отказался
                            от нескольких вышестоящих постов (например, поста заведующего
                            районным отделом народного образования), был награжден Орденом
                            Почета и орденом «Знак Почета», получил звание Заслуженного учителя
                            школы РСФСР, будучи при этом настоящим Народным учителем. Школу 654
                            и при жизни А.Д. называли школой Фридмана, это авторская школа, в
                            которой А.Д воспитал коллектив учителей-единомышленников, большая
                            часть которых – его выпускники.
                            Школа 654, руководимая А.Д Фридманом, справедливо считалась одной из
                            лучших московских школ, награждалось Почетным красным знаменем
                            Моссовета, носила звание «Образцовая школа города Москвы». А.Д. никогда
                            не кичился заслугами школы, был очень требователен к себе и коллегам,
                            анализировал недостатки в работе и строго за них спрашивал, всегда был
                            полон новых идей. Орден Почета, полученный им, он прикрепил к
                            школьному знамени…
                            Будучи тяжело больным, он продолжал руководить школой, не давая себе
                            никаких послаблений.
                            Прощаясь с Анатолием Давыдовичем, несколько сот человек, собравшихся
                            на школьном дворе, пели своему Учителю «Бригантину».
                        </p>
                        <span class="expand-btn">Подробнее..</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gui__controls">
        <ul>

            <li><a href="/">Назад</a></li>
        </ul>
    </div>
</div>
<script src="js/lib.js"></script>
<script src="js/main.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
</body>

</html>
