(function($) {
    $.fn.timeline = function() {
        var selectors = {
            id: $(this),
            item: $(this).find(".timeline-item"),
            activeClass: "timeline-item--active",
            img: ".timeline__img"
        };
        selectors.item.eq(0).addClass(selectors.activeClass);
        selectors.id.css(
            "background-image",
            "url(" +
            selectors.item
                .first()
                .find(selectors.img)
                .attr("src") +
            ")"
        );
        var itemLength = selectors.item.length;
        $(window).scroll(function() {
            var max, min;
            var pos = $(this).scrollTop();
            selectors.item.each(function(i) {
                min = $(this).offset().top;
                max = $(this).height() + $(this).offset().top;
                var that = $(this);
                if (i == itemLength - 2 && pos > min + $(this).height() / 2) {
                    selectors.item.removeClass(selectors.activeClass);
                    selectors.id.css(
                        "background-image",
                        "url(" +
                        selectors.item
                            .last()
                            .find(selectors.img)
                            .attr("src") +
                        ")"
                    );
                    selectors.item.last().addClass(selectors.activeClass);
                } else if (pos <= max - 40 && pos >= min) {
                    selectors.id.css(
                        "background-image",
                        "url(" +
                        $(this)
                            .find(selectors.img)
                            .attr("src") +
                        ")"
                    );
                    selectors.item.removeClass(selectors.activeClass);
                    $(this).addClass(selectors.activeClass);
                }
            });
        });
    };
})(jQuery);

$("#timeline-1").timeline();

$(".expand-btn").click(function(){
    $(this).siblings(".expand").toggleClass('expanded');
    $(this).text("Скрыть");
});

    var clicks = 0,
        egg = $(".egg");
    egg.click(function(){
    ++clicks;
        if (clicks > 5){egg.addClass("easter");}
    });


var checked = $(".checked");
var active = false;
$(".gui__icon-container").css("visibility","hidden");
$(".gui__history-nav").hide();

$(".section").css("display","none");
$(".section.start").css("display","block");
checked.click(function(){
    $(".section").css("display","block");
    active = true;
    $(".gui__icon-container").css("visibility","visible");

    $(this).addClass("active");
    if(active){
        new fullpage('#fullpage', {
            licenseKey: 'YOUR_KEY_HERE', 
            onLeave: function(origin, destination, direction){
                    var leavingSection = this;
            
                    if(origin.index == 0 && direction =='down'){

                        $(".gui__history-nav").show();
                        $(".gui__history-nav").css("transition","1s ease-in");
                    }
            
                    else if(origin.index == 1 && direction == 'up'){
                        $(".gui__history-nav").hide();
                        
                    }
                    
                },               
            resetSliders:true, 
            menu:".hist-nav", 
             anchors: ["home","sec","third","fourth","fifth","sixth","seventh","eightth","ninth","tenth"], 
            sectionsColor: ["","white","white","white","white","white","white","white","white","white"]});



    }
});
$(".gui_slider").css("display","none");
setTimeout(dropWelcome, 1500);
function dropWelcome(){
    $(".gui_slider").slideDown(1000);  
}
$(".close").click(function() {
    $(".gui_slider").slideUp();
    });